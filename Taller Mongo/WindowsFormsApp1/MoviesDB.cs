﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MoviesDB : Form
    {
        public MoviesDB()
        {
            InitializeComponent();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
            tabPage1.Controls.Clear();
            ViewMovTitle v = new ViewMovTitle();
            v.Dock = DockStyle.Fill;
            tabPage1.Controls.Add(v);
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {
            tabPage2.Controls.Clear();
            ViewMovFranch v = new ViewMovFranch();
            v.Dock = DockStyle.Fill;
            tabPage2.Controls.Add(v);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabPage3_Click(object sender, EventArgs e)
        {
            tabPage3.Controls.Clear();
            ViewMovYear v = new ViewMovYear();
            v.Dock = DockStyle.Fill;
            tabPage3.Controls.Add(v);
        }

        private void listView1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabPage7_Click(object sender, EventArgs e)
        {
            tabPage7.Controls.Clear();
            ViewAddProducer v = new ViewAddProducer();
            v.Dock = DockStyle.Fill;
            tabPage7.Controls.Add(v);
        }

        private void label26_Click(object sender, EventArgs e)
        {

        }

        private void label27_Click(object sender, EventArgs e)
        {

        }

        private void label28_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void textBox17_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox16_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox15_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void tabPage6_Click(object sender, EventArgs e)
        {
            tabPage6.Controls.Clear();
            ViewAddMov v = new ViewAddMov();
            v.Dock = DockStyle.Fill;
            tabPage6.Controls.Add(v);
        }

        private void tabPage5_Click(object sender, EventArgs e)
        {
            tabPage5.Controls.Clear();
            ViewStats v = new ViewStats();
            v.Dock = DockStyle.Fill;
            tabPage5.Controls.Add(v);
        }

        private void tabPage4_Click(object sender, EventArgs e)
        {
            tabPage4.Controls.Clear();
            ViewMovProducer v = new ViewMovProducer();
            v.Dock = DockStyle.Fill;
            tabPage4.Controls.Add(v);
        }

        private void tabPage1_Click_1(object sender, EventArgs e)
        {
            tabPage1.Controls.Clear();
            ViewMovTitle v = new ViewMovTitle();
            v.Dock = DockStyle.Fill;
            tabPage1.Controls.Add(v);
        }

        private void tabPage2_Click_1(object sender, EventArgs e)
        {
            tabPage2.Controls.Clear();
            ViewMovFranch v = new ViewMovFranch();
            v.Dock = DockStyle.Fill;
            tabPage2.Controls.Add(v);
        }

        private void tabPage3_Click_1(object sender, EventArgs e)
        {
            tabPage3.Controls.Clear();
            ViewMovYear v = new ViewMovYear();
            v.Dock = DockStyle.Fill;
            tabPage3.Controls.Add(v);
        }

        private void tabPage4_Click_1(object sender, EventArgs e)
        {
            tabPage4.Controls.Clear();
            ViewMovProducer v = new ViewMovProducer();
            v.Dock = DockStyle.Fill;
            tabPage4.Controls.Add(v);
        }

        private void tabPage5_Click_1(object sender, EventArgs e)
        {
            tabPage5.Controls.Clear();
            ViewStats v = new ViewStats();
            v.Dock = DockStyle.Fill;
            tabPage5.Controls.Add(v);
        }

        private void tabPage6_Click_1(object sender, EventArgs e)
        {
            tabPage6.Controls.Clear();
            ViewAddMov v = new ViewAddMov();
            v.Dock = DockStyle.Fill;
            tabPage6.Controls.Add(v);
        }

        private void tabPage7_Click_1(object sender, EventArgs e)
        {
            tabPage7.Controls.Clear();
            ViewAddProducer v = new ViewAddProducer();
            v.Dock = DockStyle.Fill;
            tabPage7.Controls.Add(v);
        }
    }
 
}
