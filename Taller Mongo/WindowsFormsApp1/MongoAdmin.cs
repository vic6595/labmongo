﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1;

namespace TallerBases
{
    static class DbHelpler
    {
        const string puerto = "mongodb://127.0.0.1:27017";
        const string dbName = "MoviesDB";




        public static bool verifyInsert(string dato, string collect)
        {

            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            var collection = db.GetCollection(collect);
            var collectionDocs = collection.FindAll();
            List<string> docsData = new List<string>();
            foreach (var doc in collectionDocs)
            {
                docsData.Add(doc.AsBsonDocument["_nombre"].ToString());

            }
            if (docsData.Contains(dato))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        internal static List<Pelicula> getMovieYear(int inicial, int final)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            MongoCollection<Pelicula> col = db.GetCollection<Pelicula>("pelicula");
            return col.Find(Query.And(Query.GTE("_ano", inicial), Query.LTE("_ano", final))).ToList();
        }

        internal static List<Pelicula> getMovieProducer(string v)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            MongoCollection<Pelicula> col = db.GetCollection<Pelicula>("pelicula");
            return col.Find(Query.EQ("_compania", v)).ToList();
        }

        internal static List<Pelicula> getMovieFranch(string franquicia)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            MongoCollection<Pelicula> col = db.GetCollection<Pelicula>("pelicula");
            return col.Find(Query.EQ("_franquicia", franquicia)).ToList();
        }

        internal static bool checkMovie(string v)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            MongoCollection<Pelicula> col = db.GetCollection<Pelicula>("pelicula");
            int cantidad = col.Find(Query.EQ("_nombre", v)).ToList().Count;
            if (cantidad != 0)
            {
                return true;
            }
            return false;
        }

        internal static List<Pelicula> getMovies()
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            MongoCollection<Pelicula> col = db.GetCollection<Pelicula>("pelicula");
            List<Pelicula> result = col.FindAll().ToList();
            return result;
        }

        public static Productora getProducerName(string nombre)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            MongoCollection<Productora> col = db.GetCollection<Productora>("Productora");
            return col.Find(Query.EQ("_nombre", nombre)).ToList().First();
        }

        public static bool insertProducer(Productora product)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);



            MongoCollection col = db.GetCollection("Productora");
            try
            {
                col.Insert<Productora>(product);
                return true;
            }
            catch
            {
                return false;
            }


        }

        internal static bool deleteProducer(string nombre)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            MongoCollection col = db.GetCollection("Productora");
            var query = new QueryDocument { { "_nombre", nombre } };
            try
            {
                col.Remove(query);
                return true;
            }
            catch
            {
                return false;
            }

        }

        public static bool insertMovie(Pelicula peli)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);



            MongoCollection col = db.GetCollection("pelicula");
            //执行插入操作
            try
            {
                col.Insert<Pelicula>(peli);
                return true;
            }
            catch
            {
                return false;
            }


        }

        public static Pelicula getMovieName(string nombre)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            MongoCollection<Pelicula> col = db.GetCollection<Pelicula>("pelicula");
            return col.Find(Query.EQ("_nombre", nombre)).ToList().First();

        }

        internal static bool deleteMovie(string nombre)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            MongoCollection col = db.GetCollection("pelicula");
            var query = new QueryDocument { { "_nombre", nombre } };
            try
            {
                col.Remove(query);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool updateProducer(Productora prod)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            MongoCollection<Productora> col = db.GetCollection<Productora>("Productora");
            var query = new QueryDocument { { "_nombre", prod._nombre } };
            Productora tmp = col.FindOneAs<Productora>(query);
            tmp._ano = prod._ano;
            tmp._web = prod._web;
            try
            {
                var result = col.Save(tmp);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool updateMovie(Pelicula peli)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            MongoCollection<Pelicula> col = db.GetCollection<Pelicula>("pelicula");
            var query = new QueryDocument { { "_nombre", peli._nombre } };
            Pelicula tmp = col.FindOneAs<Pelicula>(query);
            tmp._actores = peli._actores;
            tmp._ano = peli._ano;
            tmp._compania = peli._compania;
            tmp._director = peli._director;
            tmp._duracion = peli._duracion;
            tmp._franquicia = peli._franquicia;
            tmp._genero = peli._genero;
            tmp._pais = peli._pais;
            try
            {
                var result = col.Save(tmp);
                return true;
            }
            catch
            {
                return false;
            }


        }


        public static List<Productora> getProducer()
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(puerto);
            MongoDatabase db = server.GetDatabase(dbName);
            MongoCollection<Productora> col = db.GetCollection<Productora>("Productora");
            List<Productora> result = col.FindAll().ToList();
            return result;
        }
    }
}
