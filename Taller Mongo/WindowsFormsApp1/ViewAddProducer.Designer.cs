﻿namespace WindowsFormsApp1
{
    partial class ViewAddProducer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button8
            // 
            this.button8.AutoSize = true;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(438, 293);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(106, 35);
            this.button8.TabIndex = 48;
            this.button8.Text = "Descartar";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.AutoSize = true;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(281, 293);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(107, 35);
            this.button9.TabIndex = 47;
            this.button9.Text = "Confirmar";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(397, 239);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(198, 22);
            this.textBox17.TabIndex = 46;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Lucida Console", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(233, 239);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(61, 14);
            this.label28.TabIndex = 45;
            this.label28.Text = "Nombre";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(397, 188);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(198, 22);
            this.textBox16.TabIndex = 44;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Lucida Console", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(233, 188);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(124, 14);
            this.label27.TabIndex = 43;
            this.label27.Text = "Año Fundación";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(397, 138);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(198, 22);
            this.textBox15.TabIndex = 42;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Lucida Console", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(233, 138);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(61, 14);
            this.label26.TabIndex = 41;
            this.label26.Text = "Nombre";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Indigo;
            this.label25.Location = new System.Drawing.Point(139, 61);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(698, 17);
            this.label25.TabIndex = 40;
            this.label25.Text = "Indique los datos de la productora y presione el boton para confirmar";
            // 
            // AddProducerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.textBox17);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.textBox16);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.textBox15);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Name = "AddProducerView";
            this.Size = new System.Drawing.Size(977, 388);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
    }
}
